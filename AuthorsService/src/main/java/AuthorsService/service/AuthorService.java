package AuthorsService.service;

import AuthorsService.domain.Author;
import AuthorsService.web.model.Request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    Author getById(Long id);

    List<Author> getAll();

    Author save(AuthorRequest bookRequest);

    Author update(Long id, AuthorRequest bookRequest);

    void delete(Long id);
}
